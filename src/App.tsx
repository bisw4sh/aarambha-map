import HomePage from "./Pages/HomePage";
import NextPage from "./Pages/NextPage";
import Navbar from "./components/Navbar";

const App = () => {
  return (
    <div className="px-[4rem] py-2 max-md:px-1">
      <Navbar />
      <HomePage />
      <NextPage />
    </div>
  );
};

export default App;
