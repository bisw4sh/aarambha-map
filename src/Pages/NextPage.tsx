// import Nine20 from "../svgs/920.svg";
// import play from "../svgs/play.svg";
const NextPage = () => {
  return (
    <div className="flex min-h-screen flex-col gap-5 py-16 max-md:py-0">
      <div className="mb-[5rem] flex w-full items-center justify-between">
        <h1 className="w-1/2 text-5xl font-bold">
          Provide the best service with out of the box ideas
        </h1>
        <h2 className="text-mf w-1/2 text-zinc-400">
          we are a passionate team of digital marketing enthusiasts dedicated to
          helping businesses succeed in the digital world. With years of
          experience and a deep understanding of the ever-evolving online
          landscape, we stay at the forefront of industry trends and
          technologies.
        </h2>
      </div>
      <section className="flex items-center justify-between gap-1 max-md:w-full max-md:flex-col">
        {/* <img src="/920.png" className="h-[20rem]"></img> */}
        {/* <img src={Nine20} className="h-[20rem]"></img> */}
        <div className="relative h-[20rem] w-4/12 bg-n20 bg-contain bg-center bg-no-repeat max-md:w-full">
          {/* <img src="/avatar.png" alt="avatar" className="absolute h-[68px] bottom-[31px] left-[40px]"/> */}
        </div>
        <div className="work relative flex h-[20rem] w-2/3 items-center justify-center rounded-lg bg-zinc-300 bg-work bg-cover bg-center text-white max-md:w-full">
          <p className="text-5xl font-semibold tracking-widest">HOW WE WORK</p>
          {/* <img
            src={play}
            alt=""
            className="h-[7rem] absolute -bottom-5 -right-5"
          /> */}
          <img
            src="/play.png"
            alt=""
            className="absolute -bottom-5 -right-5 h-[7rem] cursor-pointer"
          />
        </div>
      </section>
    </div>
  );
};

export default NextPage;
