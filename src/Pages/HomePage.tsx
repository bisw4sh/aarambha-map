import LeftSection from "../components/LeftSection";
import RightSection from "../components/RightSection";

const HomePage = () => {
  return (
    <div className="flex items-center justify-center gap-[2rem] py-[6rem] max-md:flex-col">
      <LeftSection />
      <RightSection />
    </div>
  );
};

export default HomePage;
