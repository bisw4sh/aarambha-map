import { FaArrowRight } from "react-icons/fa";
import { SiAfterpay } from "react-icons/si";
import { SiBasecamp } from "react-icons/si";
import { CgMaze } from "react-icons/cg";

const LeftSection = () => {
  return (
    <main className="w-1/2 max-md:w-full">
      <section className="flex flex-col gap-1">
        <h1 className="text-7xl font-bold">
          Stay ahead of the curve with our forward-thinking
        </h1>
        <h2 className="p-2">
          An award-winning SEO agency with disciplines in digital marketing,
          design, and website development. focused on understanding you.
        </h2>

        <div className="mb-[6rem] mt-[2rem] flex items-center justify-start gap-16 max-md:justify-center">
          <button className="flex w-[13rem] items-center justify-between rounded-[30px] bg-black px-5 py-3 text-lg font-semibold text-white transition-transform duration-700 hover:scale-110">
            Scheule Call
            <span>
              <FaArrowRight />
            </span>
          </button>
          <span className="cursor-pointer font-semibold underline underline-offset-4 hover:decoration-wavy">
            View Case Study
          </span>
        </div>

        <div className="flex items-center justify-around gap-[5rem] max-md:flex-col">
          <aside className="text-md flex w-[10rem] items-center justify-center text-wrap font-semibold max-md:w-full">
            Trusted by the world's biggest brands
          </aside>
          <aside className="flex items-center justify-around gap-4 text-2xl text-zinc-500">
            <div className="flex items-center justify-center gap-1">
              <span>afterpay</span>
              <SiAfterpay />
            </div>
            <div className="flex items-center justify-center gap-1">
              <SiBasecamp />
              <span>Basecamp</span>
            </div>
            <div className="flex items-center justify-center gap-1">
              <CgMaze />
              <span>maze</span>
            </div>
          </aside>
        </div>
      </section>
    </main>
  );
};

export default LeftSection;
