import bars from "../svgs/bars.svg";
import roundUp from "../svgs/roundup.svg";

const RightSection = () => {
  return (
    <main className="flex w-1/2 flex-col items-center justify-between gap-6 py-10 max-md:w-full">
      <section className="w-[80%] max-md:w-full">
        <aside className="flex gap-4">
          <div className="blob relative flex-1 bg-[#C7C7C7]">
            <img src={roundUp} alt="" className="absolute -right-5 -top-10" />
          </div>
          <div className="flex w-1/2 flex-1 flex-col items-start justify-between gap-8 rounded-xl bg-[#F0F0F0] px-4 py-12">
            <h1 className="text-7xl font-bold">
              230<span>+</span>
            </h1>
            <p className="font-medium">
              some big companies that we work with, and trust us very much
            </p>
            <div className="relative h-2 w-full">
              <div className="absolute h-2 w-full bg-zinc-300"></div>
              <div className="absolute h-2 w-8/12 bg-black"></div>
            </div>
          </div>
        </aside>
      </section>
      <section className="w-10/12 max-md:w-full">
        {/* <img src="/bars.png" alt="bars" className="w-full h-full p-2" /> */}
        <img src={bars} alt="bars" className="p-2" />
      </section>
    </main>
  );
};

export default RightSection;
