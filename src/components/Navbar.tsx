import { TbBell } from "react-icons/tb";
import { FaAngleDown } from "react-icons/fa6";
import Logo from "../svgs/logo.svg";

const Navbar = () => {
  return (
    <div className="flex items-center justify-between">
      <div className="flex cursor-pointer items-center justify-center gap-1">
        <img src={Logo} alt="logo" />
        <span className="cursor-pointer text-xl font-bold">boostim</span>
      </div>

      <ul className="flex gap-6 max-md:hidden">
        {["Service", "Agency", "Case study", "Resources", "Contact"].map(
          (elem, idx) => {
            return (
              <li
                key={idx}
                className="flex cursor-pointer items-center justify-center gap-4 font-semibold md:gap-1"
              >
                {elem}
                <span>{elem === "Contact" ? null : <FaAngleDown />}</span>
              </li>
            );
          },
        )}
      </ul>

      <div className="flex items-center justify-center gap-4">
        <button className="rounded-[20px] px-6 py-2 font-bold ring-1 ring-black">
          Get started
        </button>
        <div className="rounded-full bg-black p-3">
          <TbBell className="stroke-4 cursor-pointer stroke-white text-2xl" />
        </div>
      </div>
    </div>
  );
};

export default Navbar;
